{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f870cf2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# pyright: analyzeUnannotatedFunctions=true"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62e19ced",
   "metadata": {},
   "source": [
    "# Assignment 4 - Predicting and understanding viewer engagement with\n",
    "educational videos\n",
    "\n",
    "With the accelerating popularity of online educational experiences, the\n",
    "role of online lectures and other educational videos continues to increase\n",
    "in scope and importance. Open access educational repositories such as\n",
    "<a href=\"http://videolectures.net/\">videolectures.net</a>, as well as\n",
    "Massive Open Online Courses (MOOCs) on platforms like Coursera, have made\n",
    "access to many thousands of lectures and tutorials an accessible option\n",
    "for millions of people around the world. Yet this impressive volume of\n",
    "content has also led to a challenge in how to find, filter, and match\n",
    "these videos with learners. This assignment gives you an example of how\n",
    "machine learning can be used to address part of that challenge.\n",
    "\n",
    "## About the prediction problem\n",
    "\n",
    "One critical property of a video is engagement: how interesting or\n",
    "\"engaging\" it is for viewers, so that they decide to keep watching.\n",
    "Engagement is critical for learning, whether the instruction is coming\n",
    "from a video or any other source. There are many ways to define\n",
    "engagement with video, but one common approach is to estimate it by\n",
    "measuring how much of the video a user watches. If the video is not\n",
    "interesting and does not engage a viewer, they will typically abandon it\n",
    "quickly, e.g. only watch 5 or 10% of the total.\n",
    "\n",
    "A first step towards providing the best-matching educational content is\n",
    "to understand which features of educational material make it engaging for\n",
    "learners in general. This is where predictive modeling can be applied,\n",
    "via supervised machine learning. For this assignment, your task is to\n",
    "predict how engaging an educational video is likely to be for viewers,\n",
    "based on a set of features extracted from the video's transcript, audio\n",
    "track, hosting site, and other sources.\n",
    "\n",
    "We chose this prediction problem for several reasons:\n",
    "\n",
    "* It combines a variety of features derived from a rich set of resources\n",
    "connected to the original data;\n",
    "* The manageable dataset size means the dataset and supervised models for\n",
    "it can be easily explored on a wide variety of computing platforms;\n",
    "* Predicting popularity or engagement for a media item, especially\n",
    "combined with understanding which features contribute to its success with\n",
    "viewers, is a fun problem but also a practical representative application\n",
    "of machine learning in a number of business and educational sectors.\n",
    "\n",
    "## About the dataset\n",
    "\n",
    "We extracted training and test datasets of educational video features\n",
    "from the VLE Dataset put together by researcher Sahan Bulathwela at\n",
    "University College London.\n",
    "\n",
    "We provide you with two data files for use in training and validating\n",
    "your models: train.csv and test.csv. Each row in these two files\n",
    "corresponds to a single educational video, and includes information about\n",
    "diverse properties of the video content as described further below. The\n",
    "target variable is `engagement` which was defined as True if the median\n",
    "percentage of the video watched across all viewers was at least 30%, and\n",
    "False otherwise.\n",
    "\n",
    "Note: Any extra variables that may be included in the training set are\n",
    "simply for your interest if you want an additional source of data for\n",
    "visualization, or to enable unsupervised and semi-supervised approaches.\n",
    "However, they are not included in the test set and thus cannot be used\n",
    "for prediction. **Only the data already included in your Coursera\n",
    "directory can be used for training the model for this assignment.**\n",
    "\n",
    "For this final assignment, you will bring together what you've learned\n",
    "across all four weeks of this course, by exploring different prediction\n",
    "models for this new dataset. In addition, we encourage you to apply what\n",
    "you've learned about model selection to do hyperparameter tuning using\n",
    "training/validation splits of the training data, to optimize the model\n",
    "and further increase its performance. In addition to a basic evaluation\n",
    "of model accuracy, we've also provided a utility function to visualize\n",
    "which features are most and least contributing to the overall model\n",
    "performance.\n",
    "\n",
    "**File descriptions**\n",
    "    assets/train.csv - the training set (Use only this data for training your model!)\n",
    "    assets/test.csv - the test set\n",
    "<br>\n",
    "\n",
    "**Data fields**\n",
    "\n",
    "train.csv & test.csv:\n",
    "\n",
    "    title_word_count - The number of words in the title of the video.\n",
    "\n",
    "    document_entropy - A score indicating how varied the topics are\n",
    "    covered in the video, based on the transcript. Videos with smaller\n",
    "    entropy scores will tend to be more cohesive and more focused on a\n",
    "    single topic.\n",
    "\n",
    "    freshness - The number of days elapsed between 01/01/1970 and the\n",
    "    lecture published date. Videos that are more recent will have higher\n",
    "    freshness values.\n",
    "\n",
    "    easiness - A text difficulty measure applied to the transcript. A\n",
    "    lower score indicates more complex language used by the presenter.\n",
    "\n",
    "    fraction_stopword_presence - A stopword is a very common word like\n",
    "    'the' or 'and'. This feature computes the fraction of all words that\n",
    "    are stopwords in the video lecture transcript.\n",
    "\n",
    "    speaker_speed - The average speaking rate in words per minute of the\n",
    "    presenter in the video.\n",
    "\n",
    "    silent_period_rate - The fraction of time in the lecture video that\n",
    "    is silence (no speaking).\n",
    "\n",
    "train.csv only:\n",
    "\n",
    "    engagement - Target label for training. True if learners watched a\n",
    "    substantial portion of the video (see description), or False\n",
    "    otherwise.\n",
    "\n",
    "## Evaluation\n",
    "\n",
    "Your predictions will be given as the probability that the corresponding\n",
    "video will be engaging to learners.\n",
    "\n",
    "The evaluation metric for this assignment is the Area Under the ROC Curve\n",
    "(AUC).\n",
    "\n",
    "Your grade will be based on the AUC score computed for your classifier. A\n",
    "model with an AUC (area under ROC curve) of at least 0.8 passes this\n",
    "assignment, and over 0.85 will receive full points.\n",
    "\n",
    "___\n",
    "\n",
    "For this assignment, create a function that trains a model to predict\n",
    "significant learner engagement with a video using `asset/train.csv`.\n",
    "Using this model, return a Pandas Series object of length 2309 with the\n",
    "data being the probability that each corresponding video from\n",
    "`readonly/test.csv` will be engaging (according to a model learned from\n",
    "the 'engagement' label in the training set), and the video index being in\n",
    "the `id` field.\n",
    "\n",
    "Example:\n",
    "\n",
    "    id\n",
    "       9240    0.401958\n",
    "       9241    0.105928\n",
    "       9242    0.018572\n",
    "                 ...\n",
    "       9243    0.208567\n",
    "       9244    0.818759\n",
    "       9245    0.018528\n",
    "             ...\n",
    "       Name: engagement, dtype: float32\n",
    "\n",
    "### Hints\n",
    "\n",
    "* Make sure your code is working before submitting it to the autograder.\n",
    "* Print out and check your result to see whether there is anything weird\n",
    "(e.g., all probabilities are the same).\n",
    "* Generally the total runtime should be less than 10 mins.\n",
    "* Try to avoid global variables. If you have other functions besides\n",
    "engagement_model, you should move those functions inside the scope of\n",
    "engagement_model.\n",
    "* Be sure to first check the pinned threads in Week 4's discussion forum\n",
    "if you run into a problem you can't figure out.\n",
    "\n",
    "### Extensions\n",
    "\n",
    "If this prediction task motivates you to explore further, you can find\n",
    "more details here on the original VLE dataset and others related to video\n",
    "engagement: https://github.com/sahanbull/VLE-Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40c4df43",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "from IPython.display import display\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.model_selection import GridSearchCV\n",
    "from sklearn.model_selection import cross_val_score\n",
    "from sklearn.preprocessing import RobustScaler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f033bce5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Do not change this value: required to be compatible with solutions\n",
    "# generated by the autograder.\n",
    "np.random.seed(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3dcb97d7",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Reading data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b962527a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def read_data():\n",
    "    df_train = pd.read_csv(\"assets/train.csv\", index_col=0)\n",
    "    df_test = pd.read_csv(\"assets/test.csv\", index_col=0)\n",
    "    return df_train, df_test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "92e81343",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_train, df_test = read_data()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ff6ab15",
   "metadata": {},
   "outputs": [],
   "source": [
    "display(df_train.head())\n",
    "display(df_train.info())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c532ce1",
   "metadata": {},
   "outputs": [],
   "source": [
    "display(df_test.head())\n",
    "display(df_test.info())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc5a2a97",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Getting features and labels from training data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e898acc",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_features_labels(df):\n",
    "    # Labels are in the last column of the DataFrame\n",
    "    X = df.iloc[:, :-1]\n",
    "    y = df.iloc[:, -1].astype(int)\n",
    "    return X, y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20313bd4",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, y_train = get_features_labels(df_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "346a12b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This dataset is IMBALANCED. The vast majority of videos are NOT ENGAGING.\n",
    "100 * y_train.value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d5b20fd",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Standardizing features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "973deaa5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def scale_features(X_train, X_test, scaler=RobustScaler()):\n",
    "    X_train_scaled = pd.DataFrame(\n",
    "        data=scaler.fit_transform(X_train),\n",
    "        columns=X_train.columns,\n",
    "        index=X_train.index,\n",
    "    )\n",
    "\n",
    "    X_test_scaled = pd.DataFrame(\n",
    "        data=scaler.transform(X_test),\n",
    "        columns=X_test.columns,\n",
    "        index=X_test.index,\n",
    "    )\n",
    "\n",
    "    return X_train_scaled, X_test_scaled"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89a6f17b",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train_scaled, X_test_scaled = scale_features(X_train, df_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "689471ea",
   "metadata": {},
   "source": [
    "## Logistic Regression"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e5516c06",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Perform grid search to find a good estimator:\n",
    "clf = LogisticRegression(\n",
    "    solver=\"liblinear\",\n",
    "    class_weight=\"balanced\",\n",
    "    random_state=33,\n",
    ")\n",
    "\n",
    "gs_lr = GridSearchCV(\n",
    "    clf,\n",
    "    param_grid={\n",
    "        \"penalty\": [\"l1\", \"l2\"],\n",
    "        \"C\": np.power(10.0, np.arange(-3, 2)),\n",
    "    },\n",
    "    scoring=\"roc_auc\",\n",
    ")\n",
    "gs_lr = gs_lr.fit(X_train_scaled, y_train)\n",
    "\n",
    "gs_lr.best_estimator_.get_params()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb5fc0c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define best estimator:\n",
    "clf = LogisticRegression(\n",
    "    penalty=\"l2\",\n",
    "    C=0.01,\n",
    "    class_weight=\"balanced\",\n",
    "    random_state=33,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "138c7285",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute AUC score by cross-validation:\n",
    "aucs = cross_val_score(\n",
    "    clf,\n",
    "    X_train_scaled,\n",
    "    y_train,\n",
    "    scoring=\"roc_auc\",\n",
    ")\n",
    "np.round(aucs, decimals=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd681fb3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Average AUC score:\n",
    "print(f\"{np.mean(aucs):.3f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "679ba1db",
   "metadata": {},
   "source": [
    "This may be good enough to pass this assignment."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0419b6b6",
   "metadata": {},
   "source": [
    "## Training the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78e515cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = clf.fit(X_train_scaled, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "558e2414",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Model coefficients:\n",
    "coeffs = (\n",
    "    pd.DataFrame(\n",
    "        data={\"coefficient\": np.squeeze(clf.coef_)},\n",
    "        index=clf.feature_names_in_,\n",
    "    )\n",
    "    .assign(abs_coefficient=lambda x: np.abs(x.coefficient))\n",
    "    .sort_values(by=\"abs_coefficient\", ascending=False)\n",
    "    .loc[:, \"coefficient\"]\n",
    "    .rename(\"Coefficients\")\n",
    ")\n",
    "coeffs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2488dcff",
   "metadata": {},
   "source": [
    "## Computing probabilities\n",
    "\n",
    "Next I'll generate the Series that the autograder expects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ea81f79",
   "metadata": {},
   "outputs": [],
   "source": [
    "ans = pd.Series(\n",
    "    data=clf.predict_proba(X_test_scaled)[:, 1],\n",
    "    index=X_test_scaled.index,\n",
    ").rename(\"engagement\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a50f37dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert len(ans) == 2309\n",
    "ans.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5dce13a0",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "The solution code needs to be in a single cell. Then I'll move all the\n",
    "relevant code into the function that follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "209f5963",
   "metadata": {},
   "outputs": [],
   "source": [
    "def engagement_model():\n",
    "    from sklearn.linear_model import LogisticRegression\n",
    "    from sklearn.preprocessing import RobustScaler\n",
    "\n",
    "    def read_data():\n",
    "        df_train = pd.read_csv(\"assets/train.csv\", index_col=0)\n",
    "        df_test = pd.read_csv(\"assets/test.csv\", index_col=0)\n",
    "        return df_train, df_test\n",
    "\n",
    "    def get_features_labels(df):\n",
    "        X = df.iloc[:, :-1]\n",
    "        y = df.iloc[:, -1].astype(int)\n",
    "        return X, y\n",
    "\n",
    "    def scale_features(X_train, X_test, scaler=RobustScaler()):\n",
    "        X_train_scaled = pd.DataFrame(\n",
    "            data=scaler.fit_transform(X_train),\n",
    "            columns=X_train.columns,\n",
    "            index=X_train.index,\n",
    "        )\n",
    "\n",
    "        X_test_scaled = pd.DataFrame(\n",
    "            data=scaler.transform(X_test),\n",
    "            columns=X_test.columns,\n",
    "            index=X_test.index,\n",
    "        )\n",
    "\n",
    "        return X_train_scaled, X_test_scaled\n",
    "\n",
    "    df_train, df_test = read_data()\n",
    "    X_train, y_train = get_features_labels(df_train)\n",
    "    X_train_scaled, X_test_scaled = scale_features(X_train, df_test)\n",
    "\n",
    "    clf = LogisticRegression(\n",
    "        penalty=\"l2\",\n",
    "        C=0.01,\n",
    "        class_weight=\"balanced\",\n",
    "    )\n",
    "    clf = clf.fit(X_train_scaled, y_train)\n",
    "\n",
    "    ans = pd.Series(\n",
    "        data=clf.predict_proba(X_test_scaled)[:, 1],\n",
    "        index=X_test_scaled.index,\n",
    "    ).rename(\"engagement\")\n",
    "    return ans"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
