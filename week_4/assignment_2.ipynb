{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a49aabf4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# pyright: analyzeUnannotatedFunctions=true"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5645877c",
   "metadata": {},
   "source": [
    "# Assignment 4 - Predicting and understanding viewer engagement with\n",
    "educational videos\n",
    "\n",
    "With the accelerating popularity of online educational experiences, the\n",
    "role of online lectures and other educational videos continues to increase\n",
    "in scope and importance. Open access educational repositories such as\n",
    "<a href=\"http://videolectures.net/\">videolectures.net</a>, as well as\n",
    "Massive Open Online Courses (MOOCs) on platforms like Coursera, have made\n",
    "access to many thousands of lectures and tutorials an accessible option\n",
    "for millions of people around the world. Yet this impressive volume of\n",
    "content has also led to a challenge in how to find, filter, and match\n",
    "these videos with learners. This assignment gives you an example of how\n",
    "machine learning can be used to address part of that challenge.\n",
    "\n",
    "## About the prediction problem\n",
    "\n",
    "One critical property of a video is engagement: how interesting or\n",
    "\"engaging\" it is for viewers, so that they decide to keep watching.\n",
    "Engagement is critical for learning, whether the instruction is coming\n",
    "from a video or any other source. There are many ways to define\n",
    "engagement with video, but one common approach is to estimate it by\n",
    "measuring how much of the video a user watches. If the video is not\n",
    "interesting and does not engage a viewer, they will typically abandon it\n",
    "quickly, e.g. only watch 5 or 10% of the total.\n",
    "\n",
    "A first step towards providing the best-matching educational content is\n",
    "to understand which features of educational material make it engaging for\n",
    "learners in general. This is where predictive modeling can be applied,\n",
    "via supervised machine learning. For this assignment, your task is to\n",
    "predict how engaging an educational video is likely to be for viewers,\n",
    "based on a set of features extracted from the video's transcript, audio\n",
    "track, hosting site, and other sources.\n",
    "\n",
    "We chose this prediction problem for several reasons:\n",
    "\n",
    "* It combines a variety of features derived from a rich set of resources\n",
    "connected to the original data;\n",
    "* The manageable dataset size means the dataset and supervised models for\n",
    "it can be easily explored on a wide variety of computing platforms;\n",
    "* Predicting popularity or engagement for a media item, especially\n",
    "combined with understanding which features contribute to its success with\n",
    "viewers, is a fun problem but also a practical representative application\n",
    "of machine learning in a number of business and educational sectors.\n",
    "\n",
    "## About the dataset\n",
    "\n",
    "We extracted training and test datasets of educational video features\n",
    "from the VLE Dataset put together by researcher Sahan Bulathwela at\n",
    "University College London.\n",
    "\n",
    "We provide you with two data files for use in training and validating\n",
    "your models: train.csv and test.csv. Each row in these two files\n",
    "corresponds to a single educational video, and includes information about\n",
    "diverse properties of the video content as described further below. The\n",
    "target variable is `engagement` which was defined as True if the median\n",
    "percentage of the video watched across all viewers was at least 30%, and\n",
    "False otherwise.\n",
    "\n",
    "Note: Any extra variables that may be included in the training set are\n",
    "simply for your interest if you want an additional source of data for\n",
    "visualization, or to enable unsupervised and semi-supervised approaches.\n",
    "However, they are not included in the test set and thus cannot be used\n",
    "for prediction. **Only the data already included in your Coursera\n",
    "directory can be used for training the model for this assignment.**\n",
    "\n",
    "For this final assignment, you will bring together what you've learned\n",
    "across all four weeks of this course, by exploring different prediction\n",
    "models for this new dataset. In addition, we encourage you to apply what\n",
    "you've learned about model selection to do hyperparameter tuning using\n",
    "training/validation splits of the training data, to optimize the model\n",
    "and further increase its performance. In addition to a basic evaluation\n",
    "of model accuracy, we've also provided a utility function to visualize\n",
    "which features are most and least contributing to the overall model\n",
    "performance.\n",
    "\n",
    "**File descriptions**\n",
    "    assets/train.csv - the training set (Use only this data for training your model!)\n",
    "    assets/test.csv - the test set\n",
    "<br>\n",
    "\n",
    "**Data fields**\n",
    "\n",
    "train.csv & test.csv:\n",
    "\n",
    "    title_word_count - The number of words in the title of the video.\n",
    "\n",
    "    document_entropy - A score indicating how varied the topics are\n",
    "    covered in the video, based on the transcript. Videos with smaller\n",
    "    entropy scores will tend to be more cohesive and more focused on a\n",
    "    single topic.\n",
    "\n",
    "    freshness - The number of days elapsed between 01/01/1970 and the\n",
    "    lecture published date. Videos that are more recent will have higher\n",
    "    freshness values.\n",
    "\n",
    "    easiness - A text difficulty measure applied to the transcript. A\n",
    "    lower score indicates more complex language used by the presenter.\n",
    "\n",
    "    fraction_stopword_presence - A stopword is a very common word like\n",
    "    'the' or 'and'. This feature computes the fraction of all words that\n",
    "    are stopwords in the video lecture transcript.\n",
    "\n",
    "    speaker_speed - The average speaking rate in words per minute of the\n",
    "    presenter in the video.\n",
    "\n",
    "    silent_period_rate - The fraction of time in the lecture video that\n",
    "    is silence (no speaking).\n",
    "\n",
    "train.csv only:\n",
    "\n",
    "    engagement - Target label for training. True if learners watched a\n",
    "    substantial portion of the video (see description), or False\n",
    "    otherwise.\n",
    "\n",
    "## Evaluation\n",
    "\n",
    "Your predictions will be given as the probability that the corresponding\n",
    "video will be engaging to learners.\n",
    "\n",
    "The evaluation metric for this assignment is the Area Under the ROC Curve\n",
    "(AUC).\n",
    "\n",
    "Your grade will be based on the AUC score computed for your classifier. A\n",
    "model with an AUC (area under ROC curve) of at least 0.8 passes this\n",
    "assignment, and over 0.85 will receive full points.\n",
    "\n",
    "___\n",
    "\n",
    "For this assignment, create a function that trains a model to predict\n",
    "significant learner engagement with a video using `asset/train.csv`.\n",
    "Using this model, return a Pandas Series object of length 2309 with the\n",
    "data being the probability that each corresponding video from\n",
    "`readonly/test.csv` will be engaging (according to a model learned from\n",
    "the 'engagement' label in the training set), and the video index being in\n",
    "the `id` field.\n",
    "\n",
    "Example:\n",
    "\n",
    "    id\n",
    "       9240    0.401958\n",
    "       9241    0.105928\n",
    "       9242    0.018572\n",
    "                 ...\n",
    "       9243    0.208567\n",
    "       9244    0.818759\n",
    "       9245    0.018528\n",
    "             ...\n",
    "       Name: engagement, dtype: float32\n",
    "\n",
    "### Hints\n",
    "\n",
    "* Make sure your code is working before submitting it to the autograder.\n",
    "* Print out and check your result to see whether there is anything weird\n",
    "(e.g., all probabilities are the same).\n",
    "* Generally the total runtime should be less than 10 mins.\n",
    "* Try to avoid global variables. If you have other functions besides\n",
    "engagement_model, you should move those functions inside the scope of\n",
    "engagement_model.\n",
    "* Be sure to first check the pinned threads in Week 4's discussion forum\n",
    "if you run into a problem you can't figure out.\n",
    "\n",
    "### Extensions\n",
    "\n",
    "If this prediction task motivates you to explore further, you can find\n",
    "more details here on the original VLE dataset and others related to video\n",
    "engagement: https://github.com/sahanbull/VLE-Dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cd56a610",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "from sklearn.ensemble import RandomForestClassifier\n",
    "from sklearn.model_selection import GridSearchCV\n",
    "from sklearn.model_selection import cross_val_score\n",
    "from sklearn.tree import DecisionTreeClassifier"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90e14e3d",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Reading data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "645fd4f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "def read_data():\n",
    "    df_train = pd.read_csv(\"assets/train.csv\", index_col=0)\n",
    "    df_test = pd.read_csv(\"assets/test.csv\", index_col=0)\n",
    "    return df_train, df_test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac12e419",
   "metadata": {},
   "outputs": [],
   "source": [
    "df_train, df_test = read_data()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b30bcbe9",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Getting features and labels from training data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c876a5a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_features_labels(df):\n",
    "    X = df.iloc[:, :-1]\n",
    "    y = df.iloc[:, -1].astype(int)\n",
    "    return X, y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecec6f58",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, y_train = get_features_labels(df_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab1b2e05",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This dataset is IMBALANCED. The vast majority of videos are NOT ENGAGING.\n",
    "100 * y_train.value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af29f067",
   "metadata": {},
   "outputs": [],
   "source": [
    "num_features = X_train.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a15422d",
   "metadata": {},
   "source": [
    "## Decision Tree Classifier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "958e887b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Perform grid search to find a good estimator:\n",
    "clf = DecisionTreeClassifier(class_weight=\"balanced\", random_state=33)\n",
    "\n",
    "depths = list(range(3, 9))\n",
    "gs = GridSearchCV(\n",
    "    clf,\n",
    "    param_grid={\n",
    "        \"max_depth\": [None, *depths],\n",
    "        \"max_features\": np.arange(1, num_features + 1),\n",
    "        # \"min_samples_split\": [100, 200, 300, 400, 500],\n",
    "    },\n",
    "    scoring=\"roc_auc\",\n",
    "    n_jobs=2,\n",
    ")\n",
    "gs = gs.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1fcbde7",
   "metadata": {},
   "outputs": [],
   "source": [
    "best_estimator = gs.best_estimator_\n",
    "best_estimator.get_params()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "269eca73",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute AUC score by cross-validation:\n",
    "aucs = cross_val_score(best_estimator, X_train, y_train, scoring=\"roc_auc\")\n",
    "np.round(aucs, decimals=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9f6b4ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Average AUC score:\n",
    "print(f\"{np.mean(aucs):.3f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08b553ea",
   "metadata": {},
   "source": [
    "### Training the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e3d075f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define best estimator:\n",
    "clf = DecisionTreeClassifier(\n",
    "    max_depth=5,\n",
    "    max_features=5,\n",
    "    class_weight=\"balanced\",\n",
    "    random_state=33,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1df3031a",
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = clf.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8bd5784b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Feature importances:\n",
    "importances = (\n",
    "    pd.Series(\n",
    "        data=clf.feature_importances_,\n",
    "        index=clf.feature_names_in_,\n",
    "    )\n",
    "    .sort_values(ascending=False)\n",
    "    .rename(\"Importances\")\n",
    ")\n",
    "importances"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b644d24",
   "metadata": {},
   "source": [
    "### Computing probabilities\n",
    "\n",
    "I've kept the following code for the sake of completeness. This model is\n",
    "slightly worse than my logistic regression classifier. So there's no\n",
    "point in submitting these probabilities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fcad71ff",
   "metadata": {},
   "outputs": [],
   "source": [
    "ans = pd.Series(\n",
    "    data=clf.predict_proba(df_test)[:, 1],\n",
    "    index=df_test.index,\n",
    ").rename(\"engagement\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72b44f88",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert len(ans) == 2309\n",
    "ans.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e6a00ea",
   "metadata": {},
   "source": [
    "## Random Forest\n",
    "\n",
    "**NOTE**: The next cell may take a while to run!!!!!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32071ded",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Perform grid search to find a good estimator:\n",
    "clf = RandomForestClassifier(class_weight=\"balanced\", random_state=33)\n",
    "\n",
    "gs = GridSearchCV(\n",
    "    clf,\n",
    "    param_grid={\n",
    "        \"n_estimators\": 50 * np.arange(2, 11),\n",
    "        \"max_depth\": np.arange(3, 9),\n",
    "        \"max_features\": np.arange(1, num_features + 1),\n",
    "    },\n",
    "    scoring=\"roc_auc\",\n",
    "    n_jobs=2,\n",
    ")\n",
    "gs = gs.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1587d706",
   "metadata": {},
   "outputs": [],
   "source": [
    "best_estimator = gs.best_estimator_\n",
    "best_estimator.get_params()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "33ca8fa1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute AUC score by cross-validation:\n",
    "aucs = cross_val_score(\n",
    "    best_estimator,\n",
    "    X_train,\n",
    "    y_train,\n",
    "    scoring=\"roc_auc\",\n",
    "    cv=10,\n",
    "    n_jobs=2,\n",
    ")\n",
    "np.round(aucs, decimals=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6261b637",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Average AUC score:\n",
    "print(f\"{np.mean(aucs):.3f}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93a4b8f6",
   "metadata": {},
   "source": [
    "### Training the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d52c9dd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define best estimator:\n",
    "clf = RandomForestClassifier(\n",
    "    n_estimators=300,\n",
    "    max_depth=8,\n",
    "    max_features=3,  # pyright: ignore\n",
    "    class_weight=\"balanced\",\n",
    "    random_state=33,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e890ac0",
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = clf.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a229d8b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Feature importances:\n",
    "importances = (\n",
    "    pd.Series(\n",
    "        data=clf.feature_importances_,\n",
    "        index=clf.feature_names_in_,\n",
    "    )\n",
    "    .sort_values(ascending=False)\n",
    "    .rename(\"Importances\")\n",
    ")\n",
    "importances"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5eaad41e",
   "metadata": {},
   "source": [
    "### Computing probabilities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3fd3c570",
   "metadata": {},
   "outputs": [],
   "source": [
    "ans = pd.Series(\n",
    "    data=clf.predict_proba(df_test)[:, 1],\n",
    "    index=df_test.index,\n",
    ").rename(\"engagement\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20e076ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert len(ans) == 2309\n",
    "ans.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e7cc79a",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "source": [
    "## Solution\n",
    "\n",
    "The solution code needs to be in a single cell. Then I'll move all the\n",
    "relevant code into the function that follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "63a4c88d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def engagement_model():\n",
    "    from sklearn.ensemble import RandomForestClassifier\n",
    "\n",
    "    def read_data():\n",
    "        df_train = pd.read_csv(\"assets/train.csv\", index_col=0)\n",
    "        df_test = pd.read_csv(\"assets/test.csv\", index_col=0)\n",
    "        return df_train, df_test\n",
    "\n",
    "    def get_features_labels(df):\n",
    "        X = df.iloc[:, :-1]\n",
    "        y = df.iloc[:, -1].astype(int)\n",
    "        return X, y\n",
    "\n",
    "    df_train, df_test = read_data()\n",
    "    X_train, y_train = get_features_labels(df_train)\n",
    "\n",
    "    clf = RandomForestClassifier(\n",
    "        n_estimators=300,\n",
    "        max_depth=8,\n",
    "        max_features=3,  # pyright: ignore\n",
    "        class_weight=\"balanced\",\n",
    "        random_state=33,\n",
    "    )\n",
    "    clf = clf.fit(X_train, y_train)\n",
    "\n",
    "    ans = pd.Series(\n",
    "        data=clf.predict_proba(df_test)[:, 1],\n",
    "        index=df_test.index,\n",
    "    ).rename(\"engagement\")\n",
    "    return ans"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "cell_metadata_filter": "-all",
   "main_language": "python",
   "notebook_metadata_filter": "-all"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
