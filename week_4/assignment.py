# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# # Assignment 4 - Predicting and understanding viewer engagement with
# educational videos
#
# With the accelerating popularity of online educational experiences, the
# role of online lectures and other educational videos continues to increase
# in scope and importance. Open access educational repositories such as
# <a href="http://videolectures.net/">videolectures.net</a>, as well as
# Massive Open Online Courses (MOOCs) on platforms like Coursera, have made
# access to many thousands of lectures and tutorials an accessible option
# for millions of people around the world. Yet this impressive volume of
# content has also led to a challenge in how to find, filter, and match
# these videos with learners. This assignment gives you an example of how
# machine learning can be used to address part of that challenge.
#
# ## About the prediction problem
#
# One critical property of a video is engagement: how interesting or
# "engaging" it is for viewers, so that they decide to keep watching.
# Engagement is critical for learning, whether the instruction is coming
# from a video or any other source. There are many ways to define
# engagement with video, but one common approach is to estimate it by
# measuring how much of the video a user watches. If the video is not
# interesting and does not engage a viewer, they will typically abandon it
# quickly, e.g. only watch 5 or 10% of the total.
#
# A first step towards providing the best-matching educational content is
# to understand which features of educational material make it engaging for
# learners in general. This is where predictive modeling can be applied,
# via supervised machine learning. For this assignment, your task is to
# predict how engaging an educational video is likely to be for viewers,
# based on a set of features extracted from the video's transcript, audio
# track, hosting site, and other sources.
#
# We chose this prediction problem for several reasons:
#
# * It combines a variety of features derived from a rich set of resources
# connected to the original data;
# * The manageable dataset size means the dataset and supervised models for
# it can be easily explored on a wide variety of computing platforms;
# * Predicting popularity or engagement for a media item, especially
# combined with understanding which features contribute to its success with
# viewers, is a fun problem but also a practical representative application
# of machine learning in a number of business and educational sectors.
#
# ## About the dataset
#
# We extracted training and test datasets of educational video features
# from the VLE Dataset put together by researcher Sahan Bulathwela at
# University College London.
#
# We provide you with two data files for use in training and validating
# your models: train.csv and test.csv. Each row in these two files
# corresponds to a single educational video, and includes information about
# diverse properties of the video content as described further below. The
# target variable is `engagement` which was defined as True if the median
# percentage of the video watched across all viewers was at least 30%, and
# False otherwise.
#
# Note: Any extra variables that may be included in the training set are
# simply for your interest if you want an additional source of data for
# visualization, or to enable unsupervised and semi-supervised approaches.
# However, they are not included in the test set and thus cannot be used
# for prediction. **Only the data already included in your Coursera
# directory can be used for training the model for this assignment.**
#
# For this final assignment, you will bring together what you've learned
# across all four weeks of this course, by exploring different prediction
# models for this new dataset. In addition, we encourage you to apply what
# you've learned about model selection to do hyperparameter tuning using
# training/validation splits of the training data, to optimize the model
# and further increase its performance. In addition to a basic evaluation
# of model accuracy, we've also provided a utility function to visualize
# which features are most and least contributing to the overall model
# performance.
#
# **File descriptions**
#     assets/train.csv - the training set (Use only this data for training your model!)
#     assets/test.csv - the test set
# <br>
#
# **Data fields**
#
# train.csv & test.csv:
#
#     title_word_count - The number of words in the title of the video.
#
#     document_entropy - A score indicating how varied the topics are
#     covered in the video, based on the transcript. Videos with smaller
#     entropy scores will tend to be more cohesive and more focused on a
#     single topic.
#
#     freshness - The number of days elapsed between 01/01/1970 and the
#     lecture published date. Videos that are more recent will have higher
#     freshness values.
#
#     easiness - A text difficulty measure applied to the transcript. A
#     lower score indicates more complex language used by the presenter.
#
#     fraction_stopword_presence - A stopword is a very common word like
#     'the' or 'and'. This feature computes the fraction of all words that
#     are stopwords in the video lecture transcript.
#
#     speaker_speed - The average speaking rate in words per minute of the
#     presenter in the video.
#
#     silent_period_rate - The fraction of time in the lecture video that
#     is silence (no speaking).
#
# train.csv only:
#
#     engagement - Target label for training. True if learners watched a
#     substantial portion of the video (see description), or False
#     otherwise.
#
# ## Evaluation
#
# Your predictions will be given as the probability that the corresponding
# video will be engaging to learners.
#
# The evaluation metric for this assignment is the Area Under the ROC Curve
# (AUC).
#
# Your grade will be based on the AUC score computed for your classifier. A
# model with an AUC (area under ROC curve) of at least 0.8 passes this
# assignment, and over 0.85 will receive full points.
#
# ___
#
# For this assignment, create a function that trains a model to predict
# significant learner engagement with a video using `asset/train.csv`.
# Using this model, return a Pandas Series object of length 2309 with the
# data being the probability that each corresponding video from
# `readonly/test.csv` will be engaging (according to a model learned from
# the 'engagement' label in the training set), and the video index being in
# the `id` field.
#
# Example:
#
#     id
#        9240    0.401958
#        9241    0.105928
#        9242    0.018572
#                  ...
#        9243    0.208567
#        9244    0.818759
#        9245    0.018528
#              ...
#        Name: engagement, dtype: float32
#
# ### Hints
#
# * Make sure your code is working before submitting it to the autograder.
# * Print out and check your result to see whether there is anything weird
# (e.g., all probabilities are the same).
# * Generally the total runtime should be less than 10 mins.
# * Try to avoid global variables. If you have other functions besides
# engagement_model, you should move those functions inside the scope of
# engagement_model.
# * Be sure to first check the pinned threads in Week 4's discussion forum
# if you run into a problem you can't figure out.
#
# ### Extensions
#
# If this prediction task motivates you to explore further, you can find
# more details here on the original VLE dataset and others related to video
# engagement: https://github.com/sahanbull/VLE-Dataset

# %%
import numpy as np
import pandas as pd
from IPython.display import display
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.preprocessing import RobustScaler

# %%
# Do not change this value: required to be compatible with solutions
# generated by the autograder.
np.random.seed(0)

# %% [markdown]
# ## Reading data


# %%
def read_data():
    df_train = pd.read_csv("assets/train.csv", index_col=0)
    df_test = pd.read_csv("assets/test.csv", index_col=0)
    return df_train, df_test


# %%
df_train, df_test = read_data()

# %%
display(df_train.head())
display(df_train.info())

# %%
display(df_test.head())
display(df_test.info())

# %% [markdown]
# ## Getting features and labels from training data


# %%
def get_features_labels(df):
    # Labels are in the last column of the DataFrame
    X = df.iloc[:, :-1]
    y = df.iloc[:, -1].astype(int)
    return X, y


# %%
X_train, y_train = get_features_labels(df_train)

# %%
# This dataset is IMBALANCED. The vast majority of videos are NOT ENGAGING.
100 * y_train.value_counts(normalize=True)

# %% [markdown]
# ## Standardizing features


# %%
def scale_features(X_train, X_test, scaler=RobustScaler()):
    X_train_scaled = pd.DataFrame(
        data=scaler.fit_transform(X_train),
        columns=X_train.columns,
        index=X_train.index,
    )

    X_test_scaled = pd.DataFrame(
        data=scaler.transform(X_test),
        columns=X_test.columns,
        index=X_test.index,
    )

    return X_train_scaled, X_test_scaled


# %%
X_train_scaled, X_test_scaled = scale_features(X_train, df_test)

# %% [markdown]
# ## Logistic Regression

# %%
# Perform grid search to find a good estimator:
clf = LogisticRegression(
    solver="liblinear",
    class_weight="balanced",
    random_state=33,
)

gs_lr = GridSearchCV(
    clf,
    param_grid={
        "penalty": ["l1", "l2"],
        "C": np.power(10.0, np.arange(-3, 2)),
    },
    scoring="roc_auc",
)
gs_lr = gs_lr.fit(X_train_scaled, y_train)

gs_lr.best_estimator_.get_params()

# %%
# Define best estimator:
clf = LogisticRegression(
    penalty="l2",
    C=0.01,
    class_weight="balanced",
    random_state=33,
)

# %%
# Compute AUC score by cross-validation:
aucs = cross_val_score(
    clf,
    X_train_scaled,
    y_train,
    scoring="roc_auc",
)
np.round(aucs, decimals=3)

# %%
# Average AUC score:
print(f"{np.mean(aucs):.3f}")

# %% [markdown]
# This may be good enough to pass this assignment.

# %% [markdown]
# ## Training the model

# %%
clf = clf.fit(X_train_scaled, y_train)

# %%
# Model coefficients:
coeffs = (
    pd.DataFrame(
        data={"coefficient": np.squeeze(clf.coef_)},
        index=clf.feature_names_in_,
    )
    .assign(abs_coefficient=lambda x: np.abs(x.coefficient))
    .sort_values(by="abs_coefficient", ascending=False)
    .loc[:, "coefficient"]
    .rename("Coefficients")
)
coeffs

# %% [markdown]
# ## Computing probabilities
#
# Next I'll generate the Series that the autograder expects.

# %%
ans = pd.Series(
    data=clf.predict_proba(X_test_scaled)[:, 1],
    index=X_test_scaled.index,
).rename("engagement")

# %%
assert len(ans) == 2309
ans.head()

# %% [markdown]
# The solution code needs to be in a single cell. Then I'll move all the
# relevant code into the function that follows.


# %%
def engagement_model():
    from sklearn.linear_model import LogisticRegression
    from sklearn.preprocessing import RobustScaler

    def read_data():
        df_train = pd.read_csv("assets/train.csv", index_col=0)
        df_test = pd.read_csv("assets/test.csv", index_col=0)
        return df_train, df_test

    def get_features_labels(df):
        X = df.iloc[:, :-1]
        y = df.iloc[:, -1].astype(int)
        return X, y

    def scale_features(X_train, X_test, scaler=RobustScaler()):
        X_train_scaled = pd.DataFrame(
            data=scaler.fit_transform(X_train),
            columns=X_train.columns,
            index=X_train.index,
        )

        X_test_scaled = pd.DataFrame(
            data=scaler.transform(X_test),
            columns=X_test.columns,
            index=X_test.index,
        )

        return X_train_scaled, X_test_scaled

    df_train, df_test = read_data()
    X_train, y_train = get_features_labels(df_train)
    X_train_scaled, X_test_scaled = scale_features(X_train, df_test)

    clf = LogisticRegression(
        penalty="l2",
        C=0.01,
        class_weight="balanced",
    )
    clf = clf.fit(X_train_scaled, y_train)

    ans = pd.Series(
        data=clf.predict_proba(X_test_scaled)[:, 1],
        index=X_test_scaled.index,
    ).rename("engagement")
    return ans
