# pyright: reportUndefinedVariable=false

# %% [markdown]
# # Module 3 Quiz

# %%
import numpy as np
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import GridSearchCV

# %% [markdown]
# ## Question 2

# %%
confusion = np.array(
    [
        [67, 11],
        [3, 8],
    ]
)
confusion

# %%
num_correct = confusion[0, 0] + confusion[1, 1]
num_predictions = np.sum(confusion)

print(num_correct)
print(num_predictions)

# %%
accuracy = num_correct / num_predictions
print(f"Accuracy: {accuracy:.3f}")

# %% [markdown]
# ## Question 3

# %%
confusion = np.array(
    [
        [102, 56],
        [17, 78],
    ]
)
confusion

# %%
tp = confusion[0, 0]

num_pos_pred = np.sum(confusion[:, 0])
num_pos_pred

# %%
precision = tp / num_pos_pred
print(f"Precision: {precision:.3f}")

# %% [markdown]
# ## Question 4

# %%
num_pos = np.sum(confusion[0, :])
num_pos

# %%
recall = tp / num_pos
print(f"Recall: {recall:.3f}")

# %% [markdown]
# **DON'T RUN THE FOLLOWING CELLS!!!!!**
#
# This code is meant to be executed inside Coursera's environment.

# %% [markdown]
# ## Question 5

# %%
probs = m.predict_proba(X_test)[:, 1]
precisions, recalls, _ = precision_recall_curve(y_test, probs)

precision = precisions[np.abs(recalls - 0.8).argmin()]
print(precision)
# 0.55 -> 0.6

# %% [markdown]
# ## Question 8

# %%
print(m.score(X_test, y_test))
# 0.744

# %% [markdown]
# ## Question 13

# %%
gs = GridSearchCV(
    m,
    param_grid={
        "C": [0.01, 0.1, 1, 10],
        "gamma": [0.01, 0.1, 1, 10],
    },
    scoring="recall",
)
gs = gs.fit(X_train, y_train)

best_estimator = gs.best_estimator_
y_pred = best_estimator.predict(X_test)

recall = recall_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)

print("{diff:.3f}".format(diff=recall - precision))
# 0.520

# %% [markdown]
# ## Question 14

# %%
gs = GridSearchCV(
    m,
    param_grid={
        "C": [0.01, 0.1, 1, 10],
        "gamma": [0.01, 0.1, 1, 10],
    },
    scoring="precision",
)
gs = gs.fit(X_train, y_train)

best_estimator = gs.best_estimator_
y_pred = best_estimator.predict(X_test)

recall = recall_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)

print("{diff:.3f}".format(diff=precision - recall))
# 0.150
