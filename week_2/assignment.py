# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# # Assignment 2
#
# In this assignment you'll explore the relationship between model
# complexity and generalization performance, by adjusting key parameters of
# various supervised learning models. Part 1 of this assignment will look
# at regression and Part 2 will look at classification.
#
# ## Part 1 - Regression

# %%
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

np.random.seed(0)
n = 15
x = np.linspace(0, 10, n) + np.random.randn(n) / 5
y = np.sin(x) + x / 6 + np.random.randn(n) / 10

X_train, X_test, y_train, y_test = train_test_split(x, y, random_state=0)


def intro():
    plt.figure()
    plt.scatter(X_train, y_train, label="training data")
    plt.scatter(X_test, y_test, label="test data")
    plt.legend(loc=4)


intro()

# %% [markdown]
# ### Question 1
#
# Write a function that fits a polynomial `LinearRegression` model on the
# *training data* `X_train` for degrees 1, 3, 6, and 9. (Use
# `PolynomialFeatures` in `sklearn.preprocessing` to create the polynomial
# features and then fit a linear regression model.) For each model, find
# 100 predicted values over the interval x = 0 to 10 (e.g. `np.linspace(0,
# 10, 100)`) and store this in a numpy array. The first row of this array
# should correspond to the output from the model trained on degree 1, the
# second row degree 3, the third row degree 6, and the fourth row degree 9.
#
# <img src="assets/polynomialreg1.png" style="width: 1000px;"/>
#
# The figure above shows the fitted models plotted on top of the original
# data (using `plot_one()`).
#
# *This function should return a numpy array with shape `(4, 100)`*


# %%
def answer_one():
    from sklearn.linear_model import LinearRegression
    from sklearn.preprocessing import PolynomialFeatures

    degree_predictions = np.zeros((4, 100))

    X_train_old = X_train.reshape(-1, 1)
    x_100 = np.linspace(0, 10, 100).reshape(-1, 1)

    for i, degree in enumerate([1, 3, 6, 9]):
        poly = PolynomialFeatures(degree)
        X_train_new = poly.fit_transform(X_train_old)

        reg = LinearRegression().fit(X_train_new, y_train)

        X_test_new = poly.transform(x_100)
        degree_predictions[i, :] = reg.predict(X_test_new)

    return degree_predictions


# %%
# Feel free to use the function plot_one() to replicate the figure from the
# prompt once you have completed question one.
def plot_one(degree_predictions):
    plt.figure(figsize=(10, 5))
    plt.plot(X_train, y_train, "o", label="training data", markersize=10)
    plt.plot(X_test, y_test, "o", label="test data", markersize=10)
    for i, degree in enumerate([1, 3, 6, 9]):
        plt.plot(
            np.linspace(0, 10, 100),
            degree_predictions[i],
            alpha=0.8,
            lw=2,
            label="degree={}".format(degree),
        )
    plt.ylim(-1, 2.5)
    plt.legend(loc=4)


plot_one(answer_one())

# %% [markdown]
# ### Question 2
#
# Write a function that fits a polynomial `LinearRegression` model on the
# training data `X_train` for degrees 0 through 9. For each model compute
# the $R^2$ (coefficient of determination) regression score on the training
# data as well as the test data, and return both of these arrays in a
# tuple.
#
# *This function should return a tuple of numpy arrays `(r2_train,
# r2_test)`. Both arrays should have shape `(10,)`*


# %%
def answer_two():
    from sklearn.linear_model import LinearRegression
    from sklearn.metrics import r2_score
    from sklearn.preprocessing import PolynomialFeatures

    r2_train = np.zeros((10,))
    r2_test = np.zeros((10,))

    X_train_old = X_train.reshape(-1, 1)
    X_test_old = X_test.reshape(-1, 1)

    for degree in range(10):
        poly = PolynomialFeatures(degree)
        X_train_new = poly.fit_transform(X_train_old)
        X_test_new = poly.transform(X_test_old)

        reg = LinearRegression().fit(X_train_new, y_train)

        y_pred = reg.predict(X_train_new)
        r2_train[degree] = r2_score(y_train, y_pred)

        y_pred = reg.predict(X_test_new)
        r2_test[degree] = r2_score(y_test, y_pred)

    return r2_train, r2_test


# %%
r2_train, r2_test = answer_two()

print(r2_train)
print(r2_train.shape)

print(r2_test)
print(r2_test.shape)

# %% [markdown]
# ### Question 3
#
# Based on the $R^2$ scores from question 2 (degree levels 0 through 9),
# what degree level corresponds to a model that is underfitting? What
# degree level corresponds to a model that is overfitting? What choice of
# degree level would provide a model with good generalization performance
# on this dataset?
#
# (Hint: Try plotting the $R^2$ scores from question 2 to visualize the
# relationship)
#
# *This function should return a tuple with the degree values in this
# order: `(Underfitting, Overfitting, Good_Generalization)`*

# %%
from typing import cast

from matplotlib.axes import Axes

_, ax = plt.subplots()
ax = cast(Axes, ax)

degrees = list(range(10))

ax.scatter(degrees, r2_train, label="training data")
ax.scatter(degrees, r2_test, label="test data")
ax.set_title("R^2 scores")
ax.legend(loc="upper left")

plt.xticks(degrees)
plt.show()


# %%
def answer_three():
    return (0, 9, 5)


# %% [markdown]
# ### Question 4
#
# Training models on high degree polynomial features can result in
# overfitting. Train two models: a non-regularized `LinearRegression` model
# and a Lasso regression model (with parameters `alpha=0.01`,
# `max_iter=10000`, `tol=0.1`) on polynomial features of degree 12. Return
# the $R^2$ score for `LinearRegression` and Lasso model's test sets.
#
# *This function should return a tuple `(LinearRegression_R2_test_score,
# Lasso_R2_test_score)`*


# %%
def answer_four():
    from sklearn.linear_model import Lasso
    from sklearn.linear_model import LinearRegression
    from sklearn.metrics import r2_score
    from sklearn.preprocessing import PolynomialFeatures

    X_train_old = X_train.reshape(-1, 1)
    X_test_old = X_test.reshape(-1, 1)

    poly = PolynomialFeatures(12)
    X_train_new = poly.fit_transform(X_train_old)
    X_test_new = poly.transform(X_test_old)

    lin_reg = LinearRegression()
    lin_reg = lin_reg.fit(X_train_new, y_train)
    y_pred = lin_reg.predict(X_test_new)
    r2_lin_reg = r2_score(y_test, y_pred)

    lasso = Lasso(alpha=0.01, max_iter=10000, tol=0.1)
    lasso = lasso.fit(X_train_new, y_train)
    y_pred = lasso.predict(X_test_new)
    r2_lasso = r2_score(y_test, y_pred)

    return r2_lin_reg, r2_lasso


# %%
r2_lin_reg, r2_lasso = answer_four()
print(r2_lin_reg)
print(r2_lasso)

# %% [markdown]
# ## Part 2 - Classification
#
# For this section of the assignment we will be working with the [UCI
# Mushroom Data Set](http://archive.ics.uci.edu/ml/datasets/Mushroom?ref=datanews.io)
# stored in `mushrooms.csv`. The data will be used to train a model to
# predict whether or not a mushroom is poisonous. The following attributes
# are provided:
#
# *Attribute Information:*
#
# 1. cap-shape: bell=b, conical=c, convex=x, flat=f, knobbed=k, sunken=s
# 2. cap-surface: fibrous=f, grooves=g, scaly=y, smooth=s
# 3. cap-color: brown=n, buff=b, cinnamon=c, gray=g, green=r, pink=p, purple=u, red=e, white=w, yellow=y
# 4. bruises?: bruises=t, no=f
# 5. odor: almond=a, anise=l, creosote=c, fishy=y, foul=f, musty=m, none=n, pungent=p, spicy=s
# 6. gill-attachment: attached=a, descending=d, free=f, notched=n
# 7. gill-spacing: close=c, crowded=w, distant=d
# 8. gill-size: broad=b, narrow=n
# 9. gill-color: black=k, brown=n, buff=b, chocolate=h, gray=g, green=r, orange=o, pink=p, purple=u, red=e, white=w, yellow=y
# 10. stalk-shape: enlarging=e, tapering=t
# 11. stalk-root: bulbous=b, club=c, cup=u, equal=e, rhizomorphs=z, rooted=r, missing=?
# 12. stalk-surface-above-ring: fibrous=f, scaly=y, silky=k, smooth=s
# 13. stalk-surface-below-ring: fibrous=f, scaly=y, silky=k, smooth=s
# 14. stalk-color-above-ring: brown=n, buff=b, cinnamon=c, gray=g, orange=o, pink=p, red=e, white=w, yellow=y
# 15. stalk-color-below-ring: brown=n, buff=b, cinnamon=c, gray=g, orange=o, pink=p, red=e, white=w, yellow=y
# 16. veil-type: partial=p, universal=u
# 17. veil-color: brown=n, orange=o, white=w, yellow=y
# 18. ring-number: none=n, one=o, two=t
# 19. ring-type: cobwebby=c, evanescent=e, flaring=f, large=l, none=n, pendant=p, sheathing=s, zone=z
# 20. spore-print-color: black=k, brown=n, buff=b, chocolate=h, green=r, orange=o, purple=u, white=w, yellow=y
# 21. population: abundant=a, clustered=c, numerous=n, scattered=s, several=v, solitary=y
# 22. habitat: grasses=g, leaves=l, meadows=m, paths=p, urban=u, waste=w, woods=d
#
# The data in the mushrooms dataset is currently encoded with strings.
# These values will need to be encoded to numeric to work with sklearn.
# We'll use pd.get_dummies to convert the categorical variables into
# indicator variables.

# %%
mush_df = pd.read_csv("assets/mushrooms.csv")
mush_df2 = pd.get_dummies(mush_df)

X_mush = mush_df2.iloc[:, 2:]
y_mush = mush_df2.iloc[:, 1]

X_train2, X_test2, y_train2, y_test2 = train_test_split(
    X_mush, y_mush, random_state=0
)

# %% [markdown]
# ### Question 5
#
# Using `X_train` and `y_train` from the preceeding cell, train a
# `DecisionTreeClassifier` with default parameters and `random_state=0`.
# What are the 5 most important features found by the decision tree?
#
# *This function should return a list of length 5 of the feature names in
# descending order of importance.*


# %%
def answer_five():
    from sklearn.tree import DecisionTreeClassifier

    clf = DecisionTreeClassifier(random_state=0)
    clf = clf.fit(X_train2, y_train2)

    idx = np.argsort(clf.feature_importances_)
    idx_5 = np.flip(idx)[:5]

    return clf.feature_names_in_[idx_5].tolist()


# %%
answer_five()

# %% [markdown]
# ### Question 6
#
# For this question, use the `validation_curve` function in
# `sklearn.model_selection` to determine training and test scores for a
# Support Vector Classifier (`SVC`) with varying parameter values.
#
# Create an `SVC` with default parameters (i.e. `kernel='rbf', C=1`) and
# `random_state=0`. Recall that the kernel width of the RBF kernel is
# controlled using the `gamma` parameter. Explore the effect of `gamma` on
# classifier accuracy by using the `validation_curve` function to find the
# training and test scores for 6 values of `gamma` from `0.0001` to `10`
# (i.e. `np.logspace(-4, 1, 6)`).
#
# For each level of `gamma`, `validation_curve` will use 3-fold cross
# validation (use `cv=3, n_jobs=2` as parameters for `validation_curve`),
# returning two 6x3 (6 levels of gamma x 3 fits per level) arrays of the
# scores for the training and test sets in each fold.
#
# Find the mean score across the 3 models for each level of `gamma` for
# both arrays, creating two arrays of length 6, and return a tuple with the
# two arrays.
#
# e.g.
#
# if one of your arrays of scores is
#
#     array([[ 0.5,  0.4,  0.6],
#            [ 0.7,  0.8,  0.7],
#            [ 0.9,  0.8,  0.8],
#            [ 0.8,  0.7,  0.8],
#            [ 0.7,  0.6,  0.6],
#            [ 0.4,  0.6,  0.5]])
#
# it should then become
#
#     array([ 0.5,  0.73333333,  0.83333333,  0.76666667,  0.63333333, 0.5])
#
# *This function should return a tuple of numpy arrays `(training_scores,
# test_scores)` where each array in the tuple has shape `(6,)`.*


# %%
def answer_six():
    from sklearn.model_selection import validation_curve
    from sklearn.svm import SVC

    train_scores, test_scores = validation_curve(
        SVC(random_state=0),
        X_mush,
        y_mush,
        param_name="gamma",
        param_range=np.logspace(-4, 1, 6),
        scoring="accuracy",
        cv=3,
        n_jobs=2,
    )

    train_scores_mean = np.mean(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)

    return train_scores_mean, test_scores_mean


# %%
train_scores, test_scores = answer_six()

# %% [markdown]
# ### Question 7
#
# Based on the scores from question 6, what gamma value corresponds to a
# model that is underfitting? What gamma value corresponds to a model that
# is overfitting? What choice of gamma would provide a model with good
# generalization performance on this dataset?
#
# (Hint: Try plotting the scores from question 6 to visualize the
# relationship)
#
# *This function should return a tuple with the gamma values in this order:
# `(Underfitting, Overfitting, Good_Generalization)`*

# %%
_, ax = plt.subplots()
ax = cast(Axes, ax)

gammas = np.logspace(-4, 1, 6)

ax.scatter(gammas, train_scores, label="training data")
ax.scatter(gammas, test_scores, label="test data")
ax.set_title("Scores")
ax.legend()

plt.xscale("log")
plt.show()


# %%
def answer_seven():
    gammas = np.logspace(-4, 1, 6)
    return (gammas[0], gammas[5], gammas[3])
