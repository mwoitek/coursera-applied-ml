# pyright: analyzeUnannotatedFunctions=true

# %% [markdown]
# # Assignment 1 - Introduction to Machine Learning
#
# For this assignment, you will be using the Breast Cancer Wisconsin
# (Diagnostic) Database to create a classifier that can help diagnose
# patients. First, read through the description of the dataset (below).

# %%
import numpy as np
import pandas as pd
from sklearn.datasets import load_breast_cancer

cancer = load_breast_cancer()

# Print the data set description
print(cancer.DESCR)

# %% [markdown]
# The object returned by `load_breast_cancer()` is a scikit-learn Bunch
# object, which is similar to a dictionary.

# %%
cancer.keys()

# %% [markdown]
# ### Question 0 (Example)
#
# How many features does the breast cancer dataset have?
#
# *This function should return an integer.*


# %%
def answer_zero():
    return len(cancer.feature_names)


# %%
answer_zero()

# %% [markdown]
# ### Question 1
#
# Scikit-learn works with lists, numpy arrays, scipy-sparse matrices, and
# pandas DataFrames, so converting the dataset to a DataFrame is not
# necessary for training this model. Using a DataFrame does however help
# make many things easier such as munging data, so let's practice creating
# a classifier with a pandas DataFrame.
#
# Convert the sklearn.dataset `cancer` to a DataFrame.
#
# *This function should return a `(569, 31)` DataFrame with*
#
# *columns =*
#
#     ['mean radius', 'mean texture', 'mean perimeter', 'mean area',
#     'mean smoothness', 'mean compactness', 'mean concavity',
#     'mean concave points', 'mean symmetry', 'mean fractal dimension',
#     'radius error', 'texture error', 'perimeter error', 'area error',
#     'smoothness error', 'compactness error', 'concavity error',
#     'concave points error', 'symmetry error', 'fractal dimension error',
#     'worst radius', 'worst texture', 'worst perimeter', 'worst area',
#     'worst smoothness', 'worst compactness', 'worst concavity',
#     'worst concave points', 'worst symmetry', 'worst fractal dimension',
#     'target']
#
# *and index =*
#
#     RangeIndex(start=0, stop=569, step=1)


# %%
def answer_one():
    df = pd.DataFrame(data=cancer.data, columns=cancer.feature_names)
    df["target"] = cancer.target
    return df


# %%
df = answer_one()
df.head()

print(df.shape)
print(df.columns.to_numpy())
print(df.index)

del df

# %% [markdown]
# ### Question 2
#
# What is the class distribution? (i.e. how many instances of `malignant`
# and how many `benign`?)
#
# *This function should return a Series named `target` of length 2 with
# integer values and index =* `['malignant', 'benign']`.


# %%
def answer_two():
    df = answer_one()

    counts = df["target"].value_counts().sort_index()
    names_dict = {i: name for i, name in enumerate(cancer.target_names)}
    counts = counts.rename(names_dict)

    return counts


# %%
counts = answer_two()
print(counts)

print(counts.name)
print(len(counts))
print(counts.index.to_numpy())

del counts

# %% [markdown]
# ### Question 3
#
# Split the DataFrame into `X` (the data) and `y` (the labels).
#
# *This function should return a tuple of length 2:* `(X, y)`*, where*
# * `X` *has shape* `(569, 30)`
# * `y` *has shape* `(569,)`.


# %%
def answer_three():
    df = answer_one()

    y = df.pop("target")
    X = df

    return X, y


# %%
X, y = answer_three()

print(X.shape)
print(y.shape)

del X, y

# %% [markdown]
# ### Question 4
#
# Using `train_test_split`, split `X` and `y` into training and test sets
# `(X_train, X_test, y_train, y_test)`.
#
# **Set the random number generator state to 0 using `random_state=0` to
# make sure your results match the autograder!**
#
# *This function should return a tuple of length 4:* `(X_train, X_test,
# y_train, y_test)`*, where*
# * `X_train` *has shape* `(426, 30)`
# * `X_test` *has shape* `(143, 30)`
# * `y_train` *has shape* `(426,)`
# * `y_test` *has shape* `(143,)`

# %%
from sklearn.model_selection import train_test_split


def answer_four():
    X, y = answer_three()
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, random_state=0
    )
    return X_train, X_test, y_train, y_test


# %%
X_train, X_test, y_train, y_test = answer_four()

print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)

del X_train, X_test, y_train, y_test

# %% [markdown]
# ### Question 5
#
# Using `KNeighborsClassifier`, fit a k-nearest neighbors (knn) classifier
# with `X_train`, `y_train` and using one nearest neighbor
# (`n_neighbors=1`).
#
# *This function should return a*
# `sklearn.neighbors.classification.KNeighborsClassifier`.

# %%
from sklearn.neighbors import KNeighborsClassifier


def answer_five():
    X_train, _, y_train, _ = answer_four()
    classifier = KNeighborsClassifier(n_neighbors=1)
    return classifier.fit(X_train, y_train)


# %%
classifier = answer_five()
type(classifier)
del classifier

# %% [markdown]
# ### Question 6
#
# Using your knn classifier, predict the class label using the mean value
# for each feature.


# %%
def answer_six():
    X, _ = answer_three()
    classifier = answer_five()

    X_mean = pd.DataFrame(
        data=X.mean().to_numpy().reshape(1, -1),
        columns=X.columns,
    )
    return classifier.predict(X_mean)


# %%
answer_six()

# %% [markdown]
# ### Question 7
#
# Using your knn classifier, predict the class labels for the test set
# `X_test`.
#
# *This function should return a numpy array with shape `(143,)` and values
# either `0.0` or `1.0`.*


# %%
def answer_seven():
    _, X_test, _, _ = answer_four()
    classifier = answer_five()
    return classifier.predict(X_test)


# %%
predictions = answer_seven()
print(predictions[:10])

print(predictions.shape)
print(np.unique(predictions))

del predictions

# %% [markdown]
# ### Question 8
#
# Find the score (mean accuracy) of your knn classifier using `X_test` and
# `y_test`.
#
# *This function should return a float between 0 and 1*


# %%
def answer_eight():
    _, X_test, _, y_test = answer_four()
    classifier = answer_five()
    return classifier.score(X_test, y_test)


# %%
score = answer_eight()

print(score)
print(type(score))
assert 0 <= score <= 1

del score
